# [NEWS TROOP Discord Archiver](https://newstroop.cinimin.net/)
<img style="-webkit-user-select: none;margin: auto;" src="https://cdn.discordapp.com/attachments/637125522886885442/637165414539067412/NEWSTROOPTEST-2.gif">

## What is NEWS TROOP
[We](https://newstroop.cinimin.net/) are a news outlet like no other.

[Visit here](https://newstroop.cinimin.net/info/about.html) to read more about us.

### Index
1. [Setup (Short)](#setup-short)
2. [Setup (Long)](#setup-long)
3. [TOKEN](#bot-token)
4. [Dependencies](#dependencies)
5. [Usage](#usage)  
   5.1 [Data Cleaning](#wow-remover)  
   5.2 [Discord Json Parser](#discord-json-parser)
6. [Copyright](#copyright-and-license)


## Setup (Short)
    pip3 install discord.py

## Setup (Long)
#### Bot Token
Discord needs to know, that the program running on your computer is the one, that is your bot, so it uses a TOKEN 
to authenticate your bot.  The TOKEN must be kept secret, so I have designed the bot to read it from a separate text 
file.

Place the token in the first line of a text file and provide the absolute path to that file to line 4 in `data_loader.py`

I did not use a cryptographically secure type of file or any good stuff, so don't guard nukes 
with your discord bot, and use this code at your own risk.

#### Creating a  TOKEN
You must create an "application" through discord's api to create a token.  You can create an application at 
https://discordapp.com/developers/applications/.  There are countless online guides with better writing, than 
I can produce for further assistance, in making a discord application.

#### What to do with your TOKEN
Once you have your TOKEN, place in on the first line of a text file named info.txt in the same directory as 
NEWSTROOP_discord_bot.py, and the bot should be able to automatically read it and appear as a user in your discord 
server.

## Usage
    !hist <limit> <user#0000>

The hist command is the one and only.  The limit must be an integer less than or equal to 9223372036854775807.
user#0000 should be substituted with a user's username and identifier.

returns:  up to <limit> messages in a file named channel_messages.txt.


### Wow Remover
wow_remover.py is a script to remove all instances of a string or list of strings within a text file.
* Fill out the first four variables with the appropriate information, and comment out the reminder line (line 3).
* The output "clean file" of Wow Remover will be appended to with new data every subsequent time the program is run, so no
data is overwritten or lost. Line 10 can be uncommented to delete the clean file every subsequent time the program is run


### Discord Json Parser
discord_json_parser.py takes the directory of csv files and uses their included json descriptors to combine them all 
together into one big text file.
* Fill out the first 5 variables starting at line 4, and **comment out the reminder line (line 3).**
* If you have no channels to remove, remove the values entered in as placeholders. All 4 variables need to be
  initialized, but the last 2 do not need to contain data.
  
#### Discord Json Parser no json
This is the same program, but for datasets where the json index files have been lost, and there are only csv files that 
are not necessarily named properly.
* only the first 3 variables are intended to be configured by the user


## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">NEWS TROOP Discord Archiver</span> 
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/TommyTorty10" property="cc:attributionName" rel="cc:attributionURL">TommyTorty10</a> is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/minimaxir/textgenrnn" rel="dct:source">https://github.com/minimaxir/textgenrnn</a>.
