import json

# reminder to fill out the following variables  # coment out this line
root = 'filepath'
outfile = 'filename.txt'
channels_to_exclude_by_name = ['']  # note channels of the same name may be removed
channels_to_exclude_by_discord_id = ['']  # no duplicates will be removed with this
message_content_to_exlude = ['']  # if any of these strings are in a message, the entire message is excluded


all_data = []
if root[-1] != '/':
	root = root + '/'

with open(root+'index.json', 'r') as raw:
	index = raw.read()

raw = json.loads(index)
discord = {}

# delete channels by id and name
for discord_id, name in raw.items():
	if (str(discord_id) not in channels_to_exclude_by_discord_id) and (name not in channels_to_exclude_by_name):
		print(name, 'found')
		discord[discord_id] = name

for i in discord:
	try:
		with open(root+i+'/messages.csv', 'r') as channel:
			chan = channel.read().split('\n')
			chan = chan[1:]
			for j in chan:
				msg = j[52:-1]

				# remove extraneous commas in front of links
				if ',http' in msg:
					msg = msg[1:]

				# remove messages that contain certain strings
				if message_content_to_exlude[0]:
					for s in message_content_to_exlude:
						if (s not in msg):
							all_data.append(msg)
							print(msg)
				else:
					all_data.append(msg)
					print(msg)

	except:
		print("you deleted that file ddiscord_idn't you ;)")

print('data concatenated')

all_data.reverse()

with open(outfile, 'a+') as f:
	f.writelines('\n'.join(all_data))

print(outfile, 'created')
