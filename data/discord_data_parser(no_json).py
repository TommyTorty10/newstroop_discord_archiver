from os import walk, sep

# reminder to fill out the following variables  # coment out this line
root = '/home/jimmy/PycharmProjects/NEWSTROOP_discord_archiver/data/Discord_Daniel_messages'
outfile = 'filename.txt'
message_content_to_exlude = ['']  # if any of these strings are in a message, the entire message is excluded

all_data = []
if root[-1] != '/':
	root = root + '/'

# iterate though all files in root
for subdir, dirs, files in walk(root):
	for filename in files:
		filepath = subdir + sep + filename

		if filepath.endswith(".csv"):
			print(filepath)
			try:
				with open(filepath, 'r') as channel:
					chan = channel.read().split('\n')
					chan = chan[1:]
					for j in chan:
						msg = j[52:-1]

						# remove extraneous commas in front of links
						if ',http' in msg:
							msg = msg[1:]

						# remove messages that contain certain strings
						# for s in message_content_to_exlude:
							# if (s != '') and (s not in msg):
						all_data.append(msg)
						print(msg)
			except:
				print("you deleted that file didn't you ;)")

print('data concatenated')

all_data.reverse()
print(all_data)

with open(outfile, 'a+') as f:
	f.writelines('\n'.join(all_data))

print(outfile, 'created')
